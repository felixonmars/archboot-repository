# Archboot-Repository
PKGBUILDS of the archboot project
https://gitlab.archlinux.org/tpowa/archboot

Repository is located here:
```plaintext
[archboot]
# Europe
Server = https://archboot.com/pkg
# U.S.
Server = https://archboot.org/pkg
```
