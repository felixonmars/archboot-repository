#!/bin/bash
. /etc/archboot/defaults
PKG="archboot archboot-bootloader"
# create repository
[[ ! -d repository ]] && mkdir repository
# build packages
for i in ${PKG}; do
	cd ${i}
	updpkgsums PKGBUILD
	sudo extra-x86_64-build || exit 1
	mv *.zst ../repository
	rm -rf *.log *.efi *.sig archboot
	cd ..
done
# update db
cd repository
repo-add -q archboot.db.tar.gz $(find "./" -type f ! -name '*.sig' -name '*.zst') || exit 1
# server release
for i in *.zst; do gpg ${_GPG} $i; done
    ssh "${_SERVER}" <<EOF
    echo "Removing old files..."
    rm  public_html/pkg/*
EOF
scp * "${_SERVER}":public_html/pkg 
cd ..
# cleanup
rm -rf repository
